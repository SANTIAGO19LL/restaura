
$("#form-add-provincia").validate({
    rules: {},
    messages: {
        nombre_sl: {
            required: "Por favor ingrese el nombre de la provincia",
        },
        capital_sl: {
            required: "Por favor ingrese la capital de la provincia",
        },
        alcalde_sl: {
            required: "Por favor ingrese el alcalde de la provincia",
        },
        region_sl: {
            required: "Por favor ingrese la region de la provincia",
        },
        fecha_fundacion_sl: {
            required: "Por favor ingrese la fecha de fundacion de la provincia",
        },
    }
})

$("#form-add-cliente").validate({
    rules: {
        cedula_sl: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10,
        },
        nombre_sl: {
            required: true,
            minlength: 5,
            maxlength: 30,
        },
        correo_sl: {
            required: true,
            email: true
        },
        direccion_sl: {
            required: true,
            minlength: 5,
            maxlength: 100,
        },
        provincia_sl: {
            required: true,
        }
    },
    messages: {
        cedula_sl: {
            required: "Por favor ingrese la cedula del cliente",
            digits: "Por favor ingrese solo numeros",
            minlength: "Por favor ingrese 10 digitos",
            maxlength: "Por favor ingrese 10 digitos",
        },
        nombre_sl: {
            required: "Por favor ingrese el nombre del cliente",
            minlength: "El nombre debe tener al menos 5 caracteres",
            maxlength: "El nombre debe tener maximo 30 caracteres",
        },
        direccion_sl: {
            required: "Por favor ingrese la direccion del cliente",
            minlength: "La direccion debe tener al menos 5 caracteres",
            maxlength: "La direccion debe tener maximo 100 caracteres",
        },
        correo_sl: {
            required: "Por favor ingrese el correo del cliente",
            email: "Por favor ingrese un email valido"
        },
        provincia_sl: {
            required: "Por favor seleccione una provincia",
        }

    }
})

$("#form-add-tipo").validate({
    rules: {
        nombre_sl: {
            required: true,
            maxlength: 50,
        },
        descripcion_sl: {
            required: true,
            maxlength: 250,
        },
        fecha_sl: {
            required: true
        }
    },
    messages:{
        nombre_sl: {
            required: "Por favor ingrese el nombre del tipo",
            maxlength: "El nombre debe tener maximo 50 caracteres",
        },
        descripcion_sl: {
            required: "Por favor ingrese la descripcion del tipo",
            maxlength: "La descripcion debe tener maximo 250 caracteres",
        },
        fecha_sl: {
            required: "Por favor ingrese la fecha de registro del tipo",
        },
        imagen_sl: {
            required: "Por favor seleccione una imagen",
        }
    }
})

$("#form-add-pedido").validate({
    rules: {
        fecha_sl: {
            required: true,
        },
        estado_sl: {
            required: true,
        },
        cliente_sl: {
            required: true,
        },
        observaciones_sl: {
            required: true,
            maxlength: 250,
        },
    },
    messages:{
        fecha_sl: {
            required: "Por favor ingrese la fecha del pedido",
        },
        estado_sl: {
            required: "Por favor ingrese un estado para el pedido",
        },
        cliente_sl: {
            required: "Por favor seleccione un cliente",
        },
        observaciones_sl: {
            required: "Por favor ingrese las observaciones del pedido",
            maxlength: "Las observaciones deben tener maximo 250 caracteres",
        },
    }
})

$("#form-add-platillo").validate({
    rules: {
        nombre_sl: {
            required: true,
            maxlength: 50,
        },
        descripcion_sl: {
            required: true,
            maxlength: 250,
        },
        precio_sl: {
            required: true,
            number: true,
        },
        tipo_sl: {
            required: true,
        },
        imagen_sl: {
            required: true,
        },
    },
    messages:{
        nombre_sl: {
            required: "Por favor ingrese el nombre del platillo",
            maxlength: "El nombre debe tener maximo 50 caracteres",
        },
        descripcion_sl: {
            required: "Por favor ingrese la descripcion del platillo",
            maxlength: "La descripcion debe tener maximo 250 caracteres",
        },
        precio_sl: {
            required: "Por favor ingrese el precio del platillo",
            number: "Por favor ingrese solo numeros",
        },
        tipo_sl: {
            required: "Por favor seleccione un tipo",
        },
        imagen_sl: {
            required: "Por favor seleccione una imagen",
        },
    }
})

$("#form-add-detalle").validate({
    rules: {
        cantidad_sl: {
            required: true,
            number: true,
        },
        comentarios_sl: {
            required: true,
            maxlength: 250,
        },
        platillo_sl: {
            required: true,
        },
        pedido_sl: {
            required: true,
        },
    },
    messages:{
        cantidad_sl: {
            required: "Por favor ingrese la cantidad del detalle",
            number: "Por favor ingrese solo numeros",
        },
        comentarios_sl: {
            required: "Por favor ingrese los comentarios del detalle",
            maxlength: "Los comentarios deben tener maximo 250 caracteres",
        },
        platillo_sl: {
            required: "Por favor seleccione un platillo",
        },
        pedido_sl: {
            required: "Por favor seleccione un pedido",
        },
    }
})

$("#form-add-ingrediente").validate({
    rules: {
        nombre_sl: {
            required: true,
            maxlength: 50,
        },
        descripcion_sl: {
            required: true,
            maxlength: 250,
        },
        cantidad_sl: {
            required: true,
            number: true,
        },
        proveedor_sl: {
            required: true,
        },
        precio_sl: {
            required: true,
            number: true,
        },
        imagen_sl: {
            required: true,
        },
    },
    messages:{
        nombre_sl: {
            required: "Por favor ingrese el nombre del ingrediente",
            maxlength: "El nombre debe tener maximo 50 caracteres",
        },
        descripcion_sl: {
            required: "Por favor ingrese la descripcion del ingrediente",
            maxlength: "La descripcion debe tener maximo 250 caracteres",
        },
        cantidad_sl: {
            required: "Por favor ingrese la cantidad del ingrediente",
            number: "Por favor ingrese solo numeros",
        },
        proveedor_sl: {
            required: "Por favor seleccione un proveedor",
        },
        precio_sl: {
            required: "Por favor ingrese el precio del ingrediente",
            number: "Por favor ingrese solo numeros",
        },
        imagen_sl: {
            required: "Por favor seleccione una imagen",
        },

    }
})

$("#form-add-receta").validate({
    rules: {
        cantidad_sl: {
            required: true,
            number: true,
        },
        observaciones_sl: {
            required: true,
            maxlength: 250,
        },
        instrucciones_sl: {
            required: true,
            maxlength: 250,
        },
        ingrediente_sl: {
            required: true,
        },
        platillo_sl: {
            required: true,
        },
    },
    messages:{
        cantidad_sl: {
            required: "Por favor ingrese la cantidad de la receta",
            number: "Por favor ingrese solo numeros",
        },
        observaciones_sl: {
            required: "Por favor ingrese las observaciones de la receta",
            maxlength: "Las observaciones deben tener maximo 250 caracteres",
        },
        instrucciones_sl: {
            required: "Por favor ingrese las instrucciones de la receta",
            maxlength: "Las instrucciones deben tener maximo 250 caracteres",
        },
        ingrediente_sl: {
            required: "Por favor seleccione un ingrediente",
        },
        platillo_sl: {
            required: "Por favor seleccione un platillo",
        },
    }
})