from django.apps import AppConfig


class RestauranteslConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.RestauranteSL'
