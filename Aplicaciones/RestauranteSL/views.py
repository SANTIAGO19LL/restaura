from django.shortcuts import render, redirect
from .models import Provincias, Platillos, Clientes, Detalles, Ingredientes, Pedidos,Recetas, Tipos 
from django.contrib import messages

# Create your views here.
def listadoProvincias(request):
    provincias = Provincias.objects.all()
    return render(request, 'listado-provincias.html', {
        'provincias': provincias
    })

def guardarProvincia(request):
    nombre=request.POST['nombre_sl']
    capital=request.POST['capital_sl']
    alcalde=request.POST['alcalde_sl']
    region=request.POST['region_sl']
    fecha_fundacion=request.POST['fecha_fundacion_sl']

    nuevoProvincia = Provincias.objects.create(nombre_sl=nombre,capital_sl=capital,alcalde_sl=alcalde,region_sl=region,fecha_fundacion_sl=fecha_fundacion)
    messages.success(request, 'Provincia guardado exitosamente')
    return redirect('/')

def eliminarProvincia(request, id):
    provincia = Provincias.objects.get(id_sl=id)
    provincia.delete()
    messages.success(request, 'Provincia eliminado exitosamente')
    return redirect('/')

def editarProvincia(request, id):
    provinciaEditar = Provincias.objects.get(id_sl=id)
    return render(request, 'editar-provincia.html', {'provincia': provinciaEditar})

def procesarActualizacionProvincia(request):
    id=request.POST['id_sl']
    nombre=request.POST['nombre_sl']
    capital=request.POST['capital_sl']
    alcalde=request.POST['alcalde_sl']
    region=request.POST['region_sl']
    fecha_fundacion=request.POST['fecha_fundacion_sl']


    provinciaEditar = Provincias.objects.get(id_sl=id)

    provinciaEditar.nombre_sl = nombre
    provinciaEditar.capital_sl = capital
    provinciaEditar.alcalde_sl = alcalde
    provinciaEditar.region_sl = region
    provinciaEditar.fecha_fundacion_sl = fecha_fundacion
    provinciaEditar.save()

    messages.success(request, 'Provincia actualizado exitosamente')
    return redirect('/')



def listadoClientes(request):
    clientes = Clientes.objects.all()
    provincias = Provincias.objects.all()
    return render(request, 'listado-clientes.html', {
        'clientes': clientes,
        'provincias': provincias
    })

def guardarCliente(request):
    cedula=request.POST['cedula_sl']
    nombre=request.POST['nombre_sl']
    direccion=request.POST['direccion_sl']
    correo=request.POST['correo_sl']
    provincia=request.POST['provincia_sl']
    provinciaSeleccionada=Provincias.objects.get(id_sl=provincia)

    nuevoCliente = Clientes.objects.create(cedula_sl=cedula,nombre_sl=nombre,direccion_sl=direccion,correo_sl=correo,provincia_sl=provinciaSeleccionada)
    messages.success(request, 'Cliente guardado exitosamente')
    return redirect('/clientes')

def eliminarCliente(request, id):
    cliente = Clientes.objects.get(id_sl=id)
    cliente.delete()
    messages.success(request, 'cliente eliminado exitosamente')
    return redirect('/clientes')

def editarCliente(request, id):
    clienteEditar = Clientes.objects.get(id_sl=id)
    provincias = Provincias.objects.all()
    return render(request, 'editar-cliente.html', {'cliente': clienteEditar, 'provincias': provincias})

def procesarActualizacionCliente(request):
    id=request.POST['id_sl']
    cedula=request.POST['cedula_sl']
    nombre=request.POST['nombre_sl']
    direccion=request.POST['direccion_sl']
    correo=request.POST['correo_sl']
    provincia=request.POST['provincia_sl']
    provinciaSeleccionada=Provincias.objects.get(id_sl=provincia)

    clienteEditar = Clientes.objects.get(id_sl=id)

    clienteEditar.cedula_sl = cedula
    clienteEditar.nombre_sl = nombre
    clienteEditar.direccion_sl = direccion
    clienteEditar.correo_sl = correo
    clienteEditar.provincia_sl = provinciaSeleccionada
    clienteEditar.save()

    messages.success(request, 'Cliente actualizado exitosamente')
    return redirect('/clientes')



def listadoPedidos(request):
    pedidos = Pedidos.objects.all()
    clientes = Clientes.objects.all()
    return render(request, 'listado-pedidos.html', {
        'pedidos': pedidos,
        'clientes': clientes
    })

def guardarPedido(request):
    fecha=request.POST['fecha_sl']
    observaciones=request.POST['observaciones_sl']
    estado=request.POST['estado_sl']
    cliente=request.POST['cliente_sl']
    clienteSeleccionada=Clientes.objects.get(id_sl=cliente)

    nuevoPedido = Pedidos.objects.create(fecha_sl=fecha,observaciones_sl=observaciones,estado_sl=estado,cliente_sl=clienteSeleccionada)
    messages.success(request, 'Pedido guardado exitosamente')
    return redirect('/pedidos')

def eliminarPedido(request, id):
    pedido = Pedidos.objects.get(id_sl=id)
    pedido.delete()
    messages.success(request, 'pedido eliminado exitosamente')
    return redirect('/pedidos')

def editarPedido(request, id):
    pedidoEditar = Pedidos.objects.get(id_sl=id)
    clientes = Clientes.objects.all()
    return render(request, 'editar-pedido.html', {'pedido': pedidoEditar, 'clientes': clientes})

def procesarActualizacionPedido(request):
    id=request.POST['id_sl']
    fecha=request.POST['fecha_sl']
    observaciones=request.POST['observaciones_sl']
    estado=request.POST['estado_sl']
    cliente=request.POST['cliente_sl']
    clienteSeleccionada=Clientes.objects.get(id_sl=cliente)

    pedidoEditar = Pedidos.objects.get(id_sl=id)

    pedidoEditar.fecha_sl = fecha
    pedidoEditar.estado_sl = estado
    pedidoEditar.observaciones_sl = observaciones
    pedidoEditar.cliente_sl = clienteSeleccionada
    pedidoEditar.save()

    messages.success(request, 'Pedido actualizado exitosamente')
    return redirect('/pedidos')



def listadoTipos(request):
    tipos = Tipos.objects.all()
    return render(request, 'listado-tipos.html', {'tipos': tipos})

def guardarTipo(request):
    nombre = request.POST['nombre_sl']
    descripcion = request.POST['descripcion_sl']
    fecha = request.POST['fecha_sl']
    imagen = request.FILES.get('imagen_sl')

    nuevoTipo = Tipos.objects.create(nombre_sl=nombre, descripcion_sl=descripcion, fecha_sl=fecha,  imagen_sl=imagen)

    messages.success(request, 'Tipo creado correctamente')
    return redirect('/tipos')

def eliminarTipo(request, id):
    tipo = Tipos.objects.get(id_sl=id)
    tipo.delete()

    messages.success(request, 'Tipo eliminado correctamente')
    return redirect('/tipos')

def editarTipo(request, id):
    tipo = Tipos.objects.get(id_sl=id)
    return render(request, 'editar-tipo.html', {'tipo': tipo})

def procesarActualizacionTipo(request):
    id = request.POST['id_sl']
    nombre = request.POST['nombre_sl']
    descripcion = request.POST['descripcion_sl']
    fecha = request.POST['fecha_sl']
    imagen = request.FILES.get('imagen_sl')

    tipoEditar = Tipos.objects.get(id_sl=id)
    tipoEditar.nombre_sl = nombre
    tipoEditar.descripcion_sl = descripcion
    tipoEditar.fecha_sl = fecha
    if imagen is not None:
        tipoEditar.imagen_sl = imagen
    tipoEditar.save()

    messages.success(request, 'Tipo actualizado correctamente')
    return redirect('/tipos')



def listadoPlatillos(request):
    platillos = Platillos.objects.all()
    tipos = Tipos.objects.all()
    return render(request, 'listado-platillos.html', {'platillos': platillos, 'tipos': tipos})

def guardarPlatillo(request):
    nombre = request.POST['nombre_sl']
    precio = request.POST['precio_sl']
    descripcion = request.POST['descripcion_sl']
    tipo = request.POST['tipo_sl']
    imagen = request.FILES.get('imagen_sl')
    tipoSeleccionado = Tipos.objects.get(id_sl=tipo)


    newPlatillo = Platillos.objects.create(nombre_sl=nombre, descripcion_sl=descripcion, precio_sl=precio, imagen_sl=imagen, tipo_sl=tipoSeleccionado)

    messages.success(request, 'Platillo creado correctamente')
    return redirect('/platillos')

def eliminarPlatillo(request, id):
    platillo = Platillos.objects.get(id_sl=id)
    platillo.delete()

    messages.success(request, 'Platillo eliminado correctamente')
    return redirect('/platillos')

def editarPlatillo(request, id):
    platillo = Platillos.objects.get(id_sl=id)
    tipos = Tipos.objects.all()
    return render(request, 'editar-platillo.html', {'platillo': platillo, 'tipos': tipos})

def procesarActualizacionPlatillo(request):
    id = request.POST['id_sl']
    nombre = request.POST['nombre_sl']
    precio = request.POST['precio_sl']
    descripcion = request.POST['descripcion_sl']
    tipo = request.POST['tipo_sl']
    imagen = request.FILES.get('imagen_sl')
    tipoSeleccionado = Tipos.objects.get(id_sl=tipo)

    platilloEditar = Platillos.objects.get(id_sl=id)
    platilloEditar.nombre_sl = nombre
    platilloEditar.descripcion_sl = descripcion
    platilloEditar.precio_sl = precio
    platilloEditar.tipo_sl = tipoSeleccionado
    if imagen is not None:
        platilloEditar.imagen_sl = imagen
    platilloEditar.save()

    messages.success(request, 'Platillo actualizado correctamente')
    return redirect('/platillos')



def listadoDetalles(request):
    detalles = Detalles.objects.all()
    platillos = Platillos.objects.all()
    pedidos = Pedidos.objects.all()
    return render(request, 'listado-detalles.html', {
        'detalles':detalles,
        'platillos': platillos,
        'pedidos': pedidos
    })

def guardarDetalle(request):
    cantidad = request.POST['cantidad_sl']
    descripcion = request.POST['descripcion_sl']
    platillo = request.POST['platillo_sl']
    pedido = request.POST['pedido_sl']
    platilloSeleccionado = Platillos.objects.get(id_sl=platillo)
    pedidoSeleccionado = Pedidos.objects.get(id_sl=pedido)

    nuevoDetalle = Detalles.objects.create(cantidad_sl=cantidad, descripcion_sl=descripcion, platillo_sl=platilloSeleccionado, pedido_sl=pedidoSeleccionado)

    messages.success(request, 'Detalle creado correctamente')
    return redirect('/detalles')

def eliminarDetalle(request, id):
    detalle = Detalles.objects.get(id_sl=id)
    detalle.delete()

    messages.success(request, 'Detalle eliminado correctamente')
    return redirect('/detalles')

def editarDetalle(request, id):
    detalle = Detalles.objects.get(id_sl=id)
    platillos = Platillos.objects.all()
    pedidos = Pedidos.objects.all()
    return render(request, 'editar-detalle.html', {'detalle': detalle, 'platillos': platillos, 'pedidos': pedidos})

def procesarActualizacionDetalle(request):
    id = request.POST['id_sl']
    cantidad = request.POST['cantidad_sl']
    descripcion = request.POST['descripcion_sl']
    platillo = request.POST['platillo_sl']
    pedido = request.POST['pedido_sl']
    platilloSeleccionado = Platillos.objects.get(id_sl=platillo)
    pedidoSeleccionado = Pedidos.objects.get(id_sl=pedido)

    detalleEditar = Detalles.objects.get(id_sl=id)
    detalleEditar.cantidad_sl = cantidad
    detalleEditar.descripcion_sl = descripcion
    detalleEditar.platillo_sl = platilloSeleccionado
    detalleEditar.pedido_sl = pedidoSeleccionado
    detalleEditar.save()

    messages.success(request, 'Detalle actualizado correctamente')
    return redirect('/detalles')



def listadoIngredientes(request):
    ingredientes = Ingredientes.objects.all()
    return render(request, 'listado-ingredientes.html', {
        'ingredientes': ingredientes
    })

def guardarIngrediente(request):
    nombre = request.POST['nombre_sl']
    descripcion = request.POST['descripcion_sl']
    cantidad = request.POST['cantidad_sl']
    proveedor = request.POST['proveedor_sl']
    imagen = request.FILES.get('imagen_sl')

    nuevoIngrediente = Ingredientes.objects.create(nombre_sl=nombre, descripcion_sl=descripcion, cantidad_sl=cantidad, proveedor_sl=proveedor, imagen_sl=imagen)

    messages.success(request, 'Ingrediente creado correctamente')
    return redirect('/ingredientes')

def eliminarIngrediente(request, id):
    ingrediente = Ingredientes.objects.get(id_sl=id)
    ingrediente.delete()

    messages.success(request, 'Ingrediente eliminado correctamente')
    return redirect('/ingredientes')

def editarIngrediente(request, id):
    ingrediente = Ingredientes.objects.get(id_sl=id)
    return render(request, 'editar-ingrediente.html', {'ingrediente': ingrediente})

def procesarActualizacionIngrediente(request):
    id = request.POST['id_sl']
    nombre = request.POST['nombre_sl']
    descripcion = request.POST['descripcion_sl']
    cantidad = request.POST['cantidad_sl']
    proveedor = request.POST['proveedor_sl']
    imagen = request.FILES.get('imagen_sl')

    ingredienteEditar = Ingredientes.objects.get(id_sl=id)
    ingredienteEditar.nombre_sl = nombre
    ingredienteEditar.descripcion_sl = descripcion
    ingredienteEditar.cantidad_sl = cantidad
    ingredienteEditar.proveedor_sl = proveedor
    if imagen is not None:
        ingredienteEditar.imagen_sl = imagen
    ingredienteEditar.save()

    messages.success(request, 'Ingrediente actualizado correctamente')
    return redirect('/ingredientes')



def listadoRecetas(request):
    recetas = Recetas.objects.all()
    platillos = Platillos.objects.all()
    ingredientes = Ingredientes.objects.all()
    return render(request, 'listado-recetas.html', {
        'recetas': recetas,
        'platillos': platillos,
        'ingredientes': ingredientes
    })

def guardarReceta(request):
    descripcion = request.POST['descripcion_sl']
    instrucciones = request.POST['instrucciones_sl']
    platillo = request.POST['platillo_sl']
    ingrediente = request.POST['ingrediente_sl']
    platilloSeleccionado = Platillos.objects.get(id_sl=platillo)
    ingredienteSeleccionado = Ingredientes.objects.get(id_sl=ingrediente)

    nuevaReceta = Recetas.objects.create(descripcion_sl=descripcion, instrucciones_sl=instrucciones, platillo_sl=platilloSeleccionado, ingrediente_sl=ingredienteSeleccionado)

    messages.success(request, 'Receta creada correctamente')
    return redirect('/recetas')

def eliminarReceta(request, id):
    receta = Recetas.objects.get(id_sl=id)
    receta.delete()

    messages.success(request, 'Receta eliminada correctamente')
    return redirect('/recetas')

def editarReceta(request, id):
    receta = Recetas.objects.get(id_sl=id)
    platillos = Platillos.objects.all()
    ingredientes = Ingredientes.objects.all()

    return render(request, 'editar-receta.html', {'receta': receta, 'platillos': platillos, 'ingredientes': ingredientes})

def procesarActualizacionReceta(request):
    id = request.POST['id_sl']
    descripcion = request.POST['descripcion_sl']
    instrucciones = request.POST['instrucciones_sl']
    platillo = request.POST['platillo_sl']
    ingrediente = request.POST['ingrediente_sl']
    platilloSeleccionado = Platillos.objects.get(id_sl=platillo)
    ingredienteSeleccionado = Ingredientes.objects.get(id_sl=ingrediente)

    recetaEditar = Recetas.objects.get(id_sl=id)
    recetaEditar.descripcion_sl = descripcion
    recetaEditar.instrucciones_sl = instrucciones
    recetaEditar.platillo_sl = platilloSeleccionado
    recetaEditar.ingrediente_sl = ingredienteSeleccionado
    recetaEditar.save()

    messages.success(request, 'Receta actualizada correctamente')
    return redirect('/recetas')