from django.db import models

# Create your models here.
class Provincias(models.Model):
    id_sl = models.AutoField(primary_key=True)
    nombre_sl = models.CharField(max_length=50)
    capital_sl = models.CharField(max_length=50)
    alcalde_sl = models.CharField(max_length=50)
    region_sl = models.CharField(max_length=50)
    fecha_fundacion_sl = models.DateField(null=True)
    

    def __str__(self):
        return self.nombre_sl

class Clientes(models.Model):
    id_sl = models.AutoField(primary_key=True)
    cedula_sl = models.CharField(max_length=10)
    nombre_sl = models.CharField(max_length=50)
    direccion_sl = models.CharField(max_length=50)
    correo_sl = models.CharField(max_length=50)
    provincia_sl = models.ForeignKey(Provincias, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.nombre_sl 

class Pedidos(models.Model):
    id_sl = models.AutoField(primary_key=True)
    fecha_sl = models.DateField()
    estado_sl = models.CharField(max_length=50)
    observaciones_sl = models.CharField(max_length=250)
    cliente_sl = models.ForeignKey(Clientes, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.fecha_sl) + ' ' + self.observaciones_sl

class Tipos(models.Model):
    id_sl = models.AutoField(primary_key=True)
    nombre_sl = models.CharField(max_length=50)
    descripcion_sl = models.CharField(max_length=250)
    fecha_sl = models.DateField()
    imagen_sl = models.FileField(upload_to='tipos', null=True, blank=True)

    def __str__(self):
        return self.nombre_sl

class Platillos(models.Model):
    id_sl = models.AutoField(primary_key=True)
    nombre_sl = models.CharField(max_length=50)
    precio_sl = models.FloatField()
    descripcion_sl = models.CharField(max_length=250)
    tipo_sl = models.ForeignKey(Tipos, on_delete=models.CASCADE, null=True, blank=True)
    imagen_sl = models.FileField(upload_to='platillos', null=True, blank=True)

    def __str__(self):
        return self.nombre_sl

class Detalles(models.Model):
    id_sl = models.AutoField(primary_key=True)
    cantidad_sl = models.IntegerField()
    descripcion_sl = models. CharField(max_length=250)
    pedido_sl = models.ForeignKey(Pedidos, on_delete=models.CASCADE, null=True, blank=True)
    platillo_sl = models.ForeignKey(Platillos, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.descripcion_sl

class Ingredientes(models.Model):
    id_sl = models.AutoField(primary_key=True)
    nombre_sl = models.CharField(max_length=50)
    descripcion_sl = models.CharField(max_length=250)
    cantidad_sl = models.IntegerField()
    proveedor_sl = models.CharField(max_length=50)
    imagen_sl = models.FileField(upload_to='ingredientes', null=True, blank=True)

    def __str__(self):
        return self.nombre_sl

class Recetas(models.Model):
    id_sl = models.AutoField(primary_key=True)
    descripcion_sl = models.CharField(max_length=250)
    instrucciones_sl = models.TextField()
    platillo_sl = models.ForeignKey(Platillos, on_delete=models.CASCADE, null=True, blank=True)
    ingrediente_sl = models.ForeignKey(Ingredientes, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.platillo_sl.nombre_sl