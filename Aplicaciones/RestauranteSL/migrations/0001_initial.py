# Generated by Django 5.0.1 on 2024-01-14 23:30

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clientes',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('cedula_sl', models.CharField(max_length=10)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('direccion_sl', models.CharField(max_length=50)),
                ('correo_sl', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Ingredientes',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('descripcion_sl', models.CharField(max_length=250)),
                ('cantidad_sl', models.IntegerField()),
                ('proveedor_sl', models.CharField(max_length=50)),
                ('imagen_sl', models.FileField(blank=True, null=True, upload_to='ingredientes')),
            ],
        ),
        migrations.CreateModel(
            name='Platillos',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('precio_sl', models.FloatField()),
                ('descripcion_sl', models.CharField(max_length=250)),
                ('imagen_sl', models.FileField(blank=True, null=True, upload_to='platillos')),
            ],
        ),
        migrations.CreateModel(
            name='Provincias',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('capital_sl', models.CharField(max_length=50)),
                ('alcalde_sl', models.CharField(max_length=50)),
                ('region_sl', models.CharField(max_length=50)),
                ('fecha_fundacion', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Tipos',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('descripcion_sl', models.CharField(max_length=250)),
                ('fecha', models.DateField()),
                ('imagen_sl', models.FileField(blank=True, null=True, upload_to='tipos')),
            ],
        ),
        migrations.CreateModel(
            name='Pedidos',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('fecha_sl', models.DateField()),
                ('estado_sl', models.CharField(max_length=50)),
                ('observaciones_sl', models.CharField(max_length=250)),
                ('cliente_sl', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.clientes')),
            ],
        ),
        migrations.CreateModel(
            name='Detalles',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('cantidad_sl', models.IntegerField()),
                ('descripcion_sl', models.CharField(max_length=250)),
                ('pedido_sl', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.pedidos')),
                ('platillo_sl', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.platillos')),
            ],
        ),
        migrations.AddField(
            model_name='clientes',
            name='provincia_sl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.provincias'),
        ),
        migrations.CreateModel(
            name='Recetas',
            fields=[
                ('id_sl', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_sl', models.CharField(max_length=50)),
                ('descripcion_sl', models.CharField(max_length=250)),
                ('instrucciones_sl', models.TextField()),
                ('ingrediente_sl', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.ingredientes')),
                ('platillo_sl', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.platillos')),
            ],
        ),
        migrations.AddField(
            model_name='platillos',
            name='tipo_sl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='RestauranteSL.tipos'),
        ),
    ]
