from django.urls import path
from . import views

urlpatterns = [
    path('', views.listadoProvincias),
    path('guardarProvincia/', views.guardarProvincia),
    path('eliminarProvincia/<id>', views.eliminarProvincia),
    path('editarProvincia/<id>', views.editarProvincia),
    path('procesarActualizacionProvincia/', views.procesarActualizacionProvincia),

    path('clientes/', views.listadoClientes),
    path('guardarCliente/', views.guardarCliente),
    path('eliminarCliente/<id>', views.eliminarCliente),
    path('editarCliente/<id>', views.editarCliente),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente),

    path('pedidos/', views.listadoPedidos),
    path('guardarPedido/', views.guardarPedido),
    path('eliminarPedido/<id>', views.eliminarPedido),
    path('editarPedido/<id>', views.editarPedido),
    path('procesarActualizacionPedido/', views.procesarActualizacionPedido),

    path('tipos/', views.listadoTipos),
    path('guardarTipo/', views.guardarTipo),
    path('eliminarTipo/<id>', views.eliminarTipo),
    path('editarTipo/<id>', views.editarTipo),
    path('procesarActualizacionTipo/', views.procesarActualizacionTipo),

    path('platillos/', views.listadoPlatillos),
    path('guardarPlatillo/', views.guardarPlatillo),
    path('eliminarPlatillo/<id>', views.eliminarPlatillo),
    path('editarPlatillo/<id>', views.editarPlatillo),
    path('procesarActualizacionPlatillo/', views.procesarActualizacionPlatillo),

    path('detalles/', views.listadoDetalles),
    path('guardarDetalle/', views.guardarDetalle),
    path('eliminarDetalle/<id>', views.eliminarDetalle),
    path('editarDetalle/<id>', views.editarDetalle),
    path('procesarActualizacionDetalle/', views.procesarActualizacionDetalle),

    path('ingredientes/', views.listadoIngredientes),
    path('guardarIngrediente/', views.guardarIngrediente),
    path('eliminarIngrediente/<id>', views.eliminarIngrediente),
    path('editarIngrediente/<id>', views.editarIngrediente),
    path('procesarActualizacionIngrediente/', views.procesarActualizacionIngrediente),

    path('recetas/', views.listadoRecetas),
    path('guardarReceta/', views.guardarReceta),
    path('eliminarReceta/<id>', views.eliminarReceta),
    path('editarReceta/<id>', views.editarReceta),
    path('procesarActualizacionReceta/', views.procesarActualizacionReceta),
]