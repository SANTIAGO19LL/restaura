from django.contrib import admin
from .models import Provincias, Detalles, Clientes, Ingredientes, Pedidos, Platillos, Recetas, Tipos

# Register your models here.
admin.site.register(Provincias)
admin.site.register(Clientes)
admin.site.register(Detalles)
admin.site.register(Ingredientes)
admin.site.register(Pedidos)
admin.site.register(Platillos)
admin.site.register(Tipos)
admin.site.register(Recetas)
