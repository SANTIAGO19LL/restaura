# Generated by Django 5.0.1 on 2024-01-15 17:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('RestauranteSL', '0002_rename_fecha_tipos_fecha_sl_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='recetas',
            name='nombre_sl',
        ),
    ]
